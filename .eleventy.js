const fg = require('fast-glob');
// import "fast-glob" as fg;

// Run search for images in /images/gallery and 
const galleryImages = fg.sync(['**/images/gallery/*', '!**/_site']);
const kriraGalleryImages = fg.sync(['**/images/krira/*', '!**/_site']);


module.exports = function(eleventyConfig) {
  // manually passthrough file copy
    eleventyConfig.addPassthroughCopy("css");
    eleventyConfig.addPassthroughCopy("js");
    eleventyConfig.addPassthroughCopy("fonts");
    eleventyConfig.addPassthroughCopy("images");
  //Create collection of gallery images
    eleventyConfig.addCollection('gallery', function(collection) {
    return galleryImages;
  });
    eleventyConfig.addCollection('krira-gallery', function(collection) {
    return kriraGalleryImages;
  });

    
  // You can return your Config object (optional).
  return {
    dir: {
      passthroughCopy : "true",
      templateFormats: ["html, njk"],
      input: "src",
        output: "_site",
        include: "_includes"
    }
  };
};
