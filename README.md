# Getting Started

1. Clone the repo and go into the directory
2. Install all the dependency `npm install`
3. install eleventy globally `npm install @11ty/eleventy -g` 
4. start a dev server `eleventy --serve`

you can run eleventy like this too `npx @11ty/eleventy` (for details visit https://www.11ty.dev/docs/getting-started/)


## Readme from the theme

TITLE: 
Church - 100% Fully Responsive Free HTML5 Bootstrap Template

AUTHOR:
DESIGNED & DEVELOPED by FreeHTML5.co

Website: http://freehtml5.co/
Twitter: http://twitter.com/fh5co
Facebook: http://facebook.com/fh5co


CREDITS:

Bootstrap
http://getbootstrap.com/

jQuery
http://jquery.com/

jQuery Easing
http://gsgd.co.uk/sandbox/jquery/easing/

Modernizr
http://modernizr.com/

Google Fonts
https://www.google.com/fonts/

Icomoon
https://icomoon.io/app/

Respond JS
https://github.com/scottjehl/Respond/blob/master/LICENSE-MIT

animate.css
http://daneden.me/animate

jQuery Waypoint
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt

Owl Carousel
http://www.owlcarousel.owlgraphic.com/

Flexslider
http://flexslider.woothemes.com/

jQuery countTo
http://www.owlcarousel.owlgraphic.com/

Magnific Popup
http://dimsemenov.com/plugins/magnific-popup/

Demo Images:
http://unsplash.com

